#!/bin/bash
count=$1
if [ "$((${count} % 3 ))" -eq 0 ] && [ "$((${count} % 5 ))" -eq 0 ];
then
    echo "tomcat"
elif [ "$(( $count % 5 ))" -eq 0 ];
then
    echo "cat"
elif [ "$(( $count % 3 ))" -eq 0 ];
then
    echo "tom"
else
echo "number is not devisible by 3, 5 and 15"
fi
