#!/bin/bash
action=$1
if [[ "${action}" = "addTeam" ]]
then
    GROUP=$2
    sudo addgroup "${GROUP}"

elif [[ "${action}" = "addUser" ]]
then 
    USER=$2
    GROUP=$3
    sudo useradd -m -g "${GROUP}" "${USER}"
    sudo chmod u=rwx,g=rx "/home/$USER"
    mkdir -p "/home/$USER/team"
    mkdir -p "/home/$USER/ninja"
    sudo chmod 774 "/home/$USER/team"
    sudo chmod 777 "/home/$USER/ninja"
    fi