#!/bin/bash
action=$1


if [[ "${action}" = "addDir" ]]
then
    path=$2
    dir=$3
    mkdir -p "${path}"/"${dir}"

elif [[ "${action}" = "listFiles" ]]
then 
    path=$2
    ls -al "${path}" | grep "^-"

elif [[ "${action}" = "listDirs" ]] 
then
    path=$2
    ls -al "${path}" | grep  "^d"

elif [[ "${action}" = "listAll" ]] 
then
    path=$2

    ls -al "${path}"
elif [[ "${action}" = "deleteDir" ]]
then
     path=$2
     dir=$3
    rm -rf "${path}"/"${dir}"
elif [[ "${action}" = "addFile" ]]
then
    path=$2
    file=$3
    content=$4
    echo "${content}" > "${path}"/"${file}"
elif [[ "${action}" = "addContentToFile" ]]
then
    path=$2
    file=$3
    content=$4
    echo "${content}" >> "${path}"/"${file}"
elif [[ "${action}" = "addContentToFileBegining" ]]
then
    path=$2
    file=$3
    content=$4
    sed -i "${content}" "${path}"/"${file}"
elif [[ "${action}" = "showFileBeginingContent" ]]
then
    path=$2
    file=$3
    beginContentAt=$4     
    head -${beginContentAt} "${path}"/"${file}"


elif [[ "${action}" = "showFileEndContent" ]]
then
    path=$2
    file=$3
    endContent=$4    
    tail -${endContent} "${path}"/"${file}"
elif [[ "${action}" = "showFileContentAtLine" ]]
then 
    path=$2
    file=$3
    contentAtLine=$4
    head -${contentAtLine} "${path}"/"${file}" | tail -1
elif [[ "${action}" = "showFileContentForLineRange" ]]
then 
    path=$2
    file=$3
    lowerRange=$4
    upperRange=$5
    head  -${upperRange} "${path}"/"${file}" | tail -$(("${upperRange}" - "${lowerRange}"  + 1))


elif [[ "${action}" = "moveFile" ]]
then 
    oldFile=$2
    newFile=$3   
    mv "${oldFile}" "${newFile}"



elif [[ "${action}" = "copyFile" ]]
then 
      oldLocation=$2
      newLocation=$3
      cp -r "${oldLocation}" "${newLocation}"


elif [[ "${action}" = "clearFileContent" ]]
then 
    path=$2
    file=$3
    > "${path}"/"${file}"

elif [[ "${action}" = "deleteFile" ]]
then 
    path=$2
    file=$3   
    rm -rf "${path}"/"${file}"
fi
