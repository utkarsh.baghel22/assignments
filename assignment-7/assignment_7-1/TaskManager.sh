#!/bin/bash

addTask()
{
OLDIFS=$IFS
IFS=':'
while read taskName taskPath taskType 
do
  	echo "${taskName},${taskPath},${taskType}" >> taskManager.data
done < "${metaFile}"
IFS=$OLDIFS
}

list()
{
awk -F "," '{print $1}' taskManager.data
}

executeTask ()
{
  
}

delete()
{
task_to_del=$1
sed -i "/^$task_to_del/d" taskManager.data
}


while getopts "a:ld:e:h:" arg; 
do
case "$arg" in
     "a")
        metaFile="${OPTARG}"
        addTask "${metaFile}"
      
       ;;
     "l")
      list
      ;;

      "d")
      del="${OPTARG}"
      delete "${del}"
      ;;

      "e")
      tasktobeexecute="${OPTARG}"
       executeTask "${tasktobeexecute}"
      ;;
      "h")
    
      ;;
    
      *)
      echo "Please enter the valid option"
      ;;
      esac
done