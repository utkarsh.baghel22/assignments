#!/bin/bash
submit() 
{
        local fname=$1
        echo $fname
        while read line; do
        task_name=$(echo "$line" | awk -F "," '{print $1}')
        echo $line >> data.csv
        done < $fname
}
list(){
        awk -F "," '{print $1}' data.csv
}

delete()
{
task_to_del=$1
sed -i "/^$task_to_del/d" data.csv
}

executeTask ()
{
START_DATE=`date`
start=$(date +%s.%N)
sleep 2
        task=$1
        path=$(grep -i "$task" data.csv | awk -F ',' '{print $2;}')
        bash $path 
END_DATE=`date`
duration=$(echo "$(date +%s.%N) - $start" | bc)
        execution_time=`printf "%.2f seconds" $duration`
        echo "$task, Execution time is == $execution_time, Job started at: $START_DATE, Job End at: $END_DATE" >> tasklog.csv
}
taskhistory()
{
        task=$1
        time=$(grep -i "$task" tasklog.csv | awk '{print $0;}')
        echo "$time"
}
while getopts "a:le:d:h:" options; do
case $options in
a) fname=$OPTARG
submit $fname;;
l) list ;;
d) task_to_del=$OPTARG
delete $task_to_del ;;
e)taskexecute="${OPTARG}"
      echo "${taskexecute}"
      executeTask "${taskexecute}";;  
h)taskexecute="${OPTARG}"
      echo "${taskexecute}"
      taskhistory "${taskexecute}";;          
*)
echo "Please enter the valid option";;
esac
done